<?php
    include 'koneksi.php';
    $db = new database();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Fitness Club - Tambah Data</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
    <!-- Navbar -->
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container">
        <a class="navbar-brand" href="index.php">Fitness Club</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-item nav-link" href="index.php">Home</a>
                <a class="nav-item nav-link" href="about.php">About</a>
                <a class="nav-item nav-link active" href="member.php">Member <span class="sr-only">(current)</span></a>
            </div>
        </div>
        </div>
    </nav>
    <!-- Navbar End -->
<div class="container">
<h4 class="mt-3 mb-3">Tambah Data Member</h4>
<form action="proses.php?aksi=m_insert" method="post" enctype="multipart/form-data">
    <div class="row">
        <div class="col-md-8 col-md-offset-1">
        <div class="form-group">
            <label for="nim">ID</label>
            <input type="text" placeholder="Masukkan ID Member" id="id_member" name="id_member" class="form-control" required>
        </div>
        <div class="form-group">
            <label for="nama">Nama Member</label>
            <input type="text" placeholder="Masukkan Nama" id="nama" name="nama" class="form-control" required>
        </div>
        <div class="form-group">
            <label for="telepon">No HP</label>
            <input type="text" placeholder="Masukkan Telepon" id="telepon" name="telepon" class="form-control" required>
        </div>
        <div class="form-group">
            <label for="level">Level</label>
            <select class="form-control" name="level">            
                <option value="Baru">Baru</option>          
                <option value="Amatir">Amatir</option>
                <option value="Sedang">Sedang</option>
                <option value="Ahli">Ahli</option>
                <option value="Admin">Admin</option>
            </select>
        </div>
        <div class="form-group">
            <label for="email">Foto</label>
            <input type="file" class="form-control-file" id="file" name="file">
        </div>
        <button type="submit" class="btn btn-dark">Simpan</button>
        <a href="member.php" class="btn btn-dark">Batal</a>
        </div>
</form>
</div>
</div>
<script src="js/jquery-3.4.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>