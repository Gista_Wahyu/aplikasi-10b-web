<?php
    include 'koneksi.php';
    $db = new database();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Fitness Club - Member</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
    <!-- Navbar -->
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container">
        <a class="navbar-brand" href="index.php">Fitness Club</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-item nav-link" href="index.php">Home</a>
                <a class="nav-item nav-link" href="about.php">About</a>
                <a class="nav-item nav-link active" href="member.php">Member <span class="sr-only">(current)</span></a>
            </div>
        </div>
        </div>
    </nav>
    <!-- Navbar End -->
    <div class="container">
    <h4 class="mt-3 mb-3">Daftar Member</h4>
    <?php
    if(isset($_GET['pesan'])){
        if($_GET['pesan'] == "insertsuccess"){
            $msg = "sukses ditambahkan";
            $tipe = "success";
        }else if($_GET['pesan'] == "updatesuccess"){
            $msg = "sukses diedit";
            $tipe = "success";
        }else if($_GET['pesan'] == "deletesuccess"){
            $msg = "sukses dihapus";
            $tipe = "success";
        }else if($_GET['pesan'] == "insertfailed"){
            $msg = "gagal ditambahkan";
            $tipe = "danger";
        }else if($_GET['pesan'] == "updatefailed"){
            $msg = "gagal diedit";
            $tipe = "danger";
        }else if($_GET['pesan'] == "deletefailed"){
            $msg = "gagal dihapus";
            $tipe = "danger";
        }
        echo '<div class="alert alert-'.$tipe.' alert-dismissible fade show" role="alert">
                Data Member <strong>'.$msg.'</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
    }
    ?>
    <table class="table">
    <thead>
        <tr>
        <th scope="col">ID</th>
        <th scope="col">Nama Member</th>
        <th scope="col">No HP</th>
        <th scope="col">Level</th>
        <th scope="col">Foto</th>
        <th scope="col">Aksi</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($db->tampildata() as $mem) : ?>
        <tr>
            <td><?= $mem['id_member'] ?></td>
            <td><?= $mem['nama'] ?></td>
            <td><?= $mem['telepon'] ?></td>
            <td><?php
            if($mem['level']=='Baru'){
                echo "<img src='level/1.png' width='100px' height='25' />";
            }else if($mem['level']=='Amatir'){
                echo "<img src='level/2.png' width='100px' height='25px' />";
            }else if($mem['level']=='Sedang'){
                echo "<img src='level/3.png' width='100px' height='25px' />";
            }else if($mem['level']=='Ahli'){
                echo "<img src='level/4.png' width='100px' height='25px' />";
            }else if($mem['level']=='Admin'){
                echo "<img src='level/5.png' width='100px' height='25px' />";
            }

            ?></td>
            <td><img src="<?= $mem['url'] ?>" width="40px" height="50px" /></td>
            <td>
                <a href="update.php?id_member=<?php echo $mem['id_member']; ?>" class="btn btn-dark">Edit</a>
                <a href="proses.php?id_member=<?php echo $mem['id_member']; ?>&aksi=m_delete" class="btn btn-dark">Hapus</a>
			</td>
        </tr>
    <?php endforeach ?>
    </tbody>
    </table>
    <a href="insert.php" class="btn btn-dark mb-3">
    Tambah Member
    </a>
</div>
<script src="js/jquery-3.4.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>